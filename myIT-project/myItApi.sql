drop table if exists api.Expense, api.Debit, api.User, api.Expectation,api.Pack,api.Image;

create table api.Pack(
    id serial primary key,
    budget double not null,
    debits text,
    expectations text,
    title varchar(50),
    image_id integer references api.Image(id)
);

CREATE TABLE api.Image (
  id serial primary key,
  image_data LONGBLOB
);

create table api.Expense(
	id serial primary key,
    expectation_id integer references api.Debit(id),
    description varchar(50) not null,
    category varchar(50) not null,
    price double not null
);

create table api.Expectation(
    id serial primary key,
    name varchar(40) unique,
    prevision_expected double not null,
    pack_id integer references api.Pack(id),
    prevision_spending double default 0
);

create table api.Debit(
	id serial primary key,
    description varchar(100) not null,
    cost double not null,
    pack_id integer references api.Pack(id),
    is_debited boolean default False
);

create table api.User(
    id serial primary key,
    budget double not null,
    pack_id integer references api.Pack(id),
    savings double default 0
);



-- insert into api.Expense (description,price) values ('depense test',15.2),('depense test',16);
-- insert into api.Debit (description,price,state) values ('debit test',15.2,True),('debit test',14.2,False);
-- insert into api.User (budget,expected_bank_balance) values (600,600);
-- insert into api.Expectation (category,prevision_expected) values ('Laverie',50.4),('Mamie',50.4);