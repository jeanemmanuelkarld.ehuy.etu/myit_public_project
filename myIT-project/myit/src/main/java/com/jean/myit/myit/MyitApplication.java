package com.jean.myit.myit;
import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import com.jean.myit.myit.model.Debit;
import com.jean.myit.myit.model.Expectation;
import com.jean.myit.myit.model.Expense;

import com.jean.myit.myit.service.ExpectationService;
import com.jean.myit.myit.service.ExpenseService;



@SpringBootApplication
public class MyitApplication implements CommandLineRunner{
	@Autowired
	private ExpectationService expectationService;
	@Autowired
	private ExpenseService expenseService;
	// @Autowired
	// private PackService packService;

	public static void main(String[] args) {
		SpringApplication.run(MyitApplication.class, args);
	}

	@Override
	@Transactional
	public void run(String... args) throws Exception {
		// Pack pack = new Pack();
		
		// Expectation exp = new Expectation();
		// exp.setName("Test");
		// exp.setPrevisionExpected(0.0);
		
		// Debit deb = new Debit();
		// deb.setCost(0.0);
		// deb.setDescription("Test");
		// deb.setIsDebited(false);

	}

}
