package com.jean.myit.myit.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jean.myit.myit.model.Debit;
import com.jean.myit.myit.service.DebitService;

@RestController
public class DebitController{
    @Autowired
    private DebitService debitService;

    @GetMapping("/debits")
    public List<Debit> getDebits(){
        return debitService.getAll();
    }

    @GetMapping("debit/{id}")
    public Debit getDebit(@PathVariable("id") Integer id){
        return debitService.get(id);
    }

    @PostMapping("/debit")
    public Debit createDebit(@RequestBody Debit debit){
        Debit res = debitService.create(debit);
        return res;
    }

    // @DeleteMapping("/debits")
    // public void deleteDebits(){
    //     debitService.deleteAll();
    // }

    @DeleteMapping("/debit/{id}")
    public void deleteDebit(@PathVariable("id") Integer id){
        debitService.delete(id);
    }
    
    @PutMapping("/debit/{id}")
    public Debit updateDebit(@PathVariable("id") Integer id,@RequestBody Debit debit){
        return debitService.updateDebit(id,debit);
    }

}