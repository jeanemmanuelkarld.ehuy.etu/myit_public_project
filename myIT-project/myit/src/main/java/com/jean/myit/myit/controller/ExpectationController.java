package com.jean.myit.myit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jean.myit.myit.model.Expectation;
import com.jean.myit.myit.model.Expense;
import com.jean.myit.myit.service.ExpectationService;

@RestController
public class ExpectationController {
    @Autowired
    private ExpectationService expectationService;

    @GetMapping("/expectations")
    public List<Expectation> getExpectations(){
        return expectationService.getAll();
    }

    @GetMapping("expectation/{id}")
    public Expectation getExpectation(@PathVariable("id") Integer id){
        return expectationService.get(id);
    }

    @PostMapping("/expectation")
    public Expectation createExpectation(@RequestBody Expectation exp){
        return expectationService.create(exp);
    }
    
    @Transactional
    @PostMapping("/expectation/{id}/expense")
    public Expense addExpense(@PathVariable("id") Integer id, @RequestBody Expense exp){
        Expense res = expectationService.addExpense(id, exp);
        return res;
    }

    // @DeleteMapping("/expectations")
    // public void deleteExpectations(){
    //     expectationService.deleteAll();
    // }

    @DeleteMapping("/expectation/{id}")
    public void deleteExpectation(@PathVariable("id") Integer id){
        expectationService.delete(id);
    }

    @Transactional
    @DeleteMapping("/expectation/{expectation_category}/expense/{expense_id}")
    public void deleteExpense(@PathVariable("expectation_category") String expectation_category,@PathVariable("expense_id") Integer expense_id){
        expectationService.removeExpenseByCategory(expectation_category, expense_id);
    }



}
