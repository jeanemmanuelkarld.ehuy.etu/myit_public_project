package com.jean.myit.myit.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jean.myit.myit.model.Expense;
import com.jean.myit.myit.service.ExpenseService;

@RestController
public class ExpenseController implements CommandLineRunner{
    @Autowired
    private ExpenseService expenseService;

    @GetMapping("/expenses")
    public List<Expense> getExpenses(){
        return this.expenseService.getAll();
    }

    @GetMapping("/expense/{id}")
    public Expense getExpense(@PathVariable("id") Integer id){
        return this.expenseService.get(id);
    }

    // @PostMapping("/expense")
    // public Expense createExpense(@RequestBody Expense expense){
    //     return expenseService.create(expense);
    // }

    // @PutMapping("/expense/{id}")
    // public Expense updateExpense(@PathVariable("id") Integer id,@RequestBody Expense expense){
    //     Optional<Expense> resp = this.expenseService.getExpense(id);
    //     if(resp.isPresent()){
    //         Expense editedExp = resp.get();
    //         this.expenseService.updateExpense(editedExp, expense);
    //         this.expenseService.saveExpense(editedExp);
    //         return editedExp;
    //     }else{
    //         return null;
    //     }
    // }

    // @DeleteMapping("/expenses")
    // public void deleteExpenses(){
    //     this.expenseService.deleteExpenses();
    //     this.userService.updateExpectedBankBalance((Integer) 1);
    // }

    @DeleteMapping("/expense/{id}")
    public void deleExpense(@PathVariable("id") Integer id){
        this.expenseService.delete(id);
        
    }

    public void disp(String debug){
        try {
            this.run(debug);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run(String... args) throws Exception {
        if (args.length == 1)
            System.out.println(args[0]);
    }

}
