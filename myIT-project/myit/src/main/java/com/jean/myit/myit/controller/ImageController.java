package com.jean.myit.myit.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.jean.myit.myit.model.Image;
import com.jean.myit.myit.service.ImageService;


@RestController
public class ImageController {

    @Autowired
    private ImageService imageService;

    @PostMapping("/upload")
    public Image uploadImage(@RequestParam("image") MultipartFile image) {
        return imageService.uploadImage(image);
    }

    @GetMapping("/image/{id}")
    public byte[] getImage(@PathVariable Integer id) {
        return imageService.getImage(id);
    }
}
