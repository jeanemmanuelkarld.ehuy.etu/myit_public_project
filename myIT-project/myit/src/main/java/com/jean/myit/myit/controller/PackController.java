package com.jean.myit.myit.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jean.myit.myit.model.Pack;
import com.jean.myit.myit.service.PackService;

@RestController
public class PackController {
    
    @Autowired
    private PackService packService;

    @GetMapping("/packs")
    public List<Pack> getPacks(){
        return packService.getAll();
    }

    @GetMapping("/pack/{id}")
    public Pack getPack(@PathVariable("id") Integer id){
        return packService.get(id);
    }

    @GetMapping("/pack/initialize/{id}")
    public void initializePack(@PathVariable("id") Integer id){
        packService.initialize(id);
    }
    

    @PostMapping("/pack")
    public Pack createPack(@RequestParam("pack") String packJson, @RequestParam("image") MultipartFile image) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Pack pack = objectMapper.readValue(packJson, Pack.class);
            return packService.createPackWithImage(pack, image);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    @DeleteMapping("/pack/{id}")
    public void deletePack(@PathVariable("id") Integer id){
        packService.delete(id);
    }

}
