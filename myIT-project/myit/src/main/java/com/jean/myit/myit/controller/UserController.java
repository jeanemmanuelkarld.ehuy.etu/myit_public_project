package com.jean.myit.myit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jean.myit.myit.model.User;
import com.jean.myit.myit.service.UserService;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public List<User> getUsers(){
        return userService.getAll();
    }

    @GetMapping("/user/{id}")
    public User getUser(@PathVariable("id") Integer id){
        return userService.get(id);
    }

    @GetMapping("/user")
    public User getCurrentUser(){
        return userService.get(1);
    }

    @PostMapping("/user/budget")
    public User createUser(@RequestBody User user){
        User current = userService.createDebitBudget(1, user);
        return current;
    }

    @RequestMapping({"/user", "/user/**"})
    public User updateSavingsUser(){
        return userService.calculAndSetSavings(1);
    }

    
}