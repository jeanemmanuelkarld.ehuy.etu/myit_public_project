package com.jean.myit.myit.model;

import java.util.ArrayList;
import java.util.List;


import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="Expectation")
public class Expectation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Column(name = "prevision_expected")
    private Double previsionExpected;
    @Column(name = "prevision_spending")
    private Double previsionSpending= (double) 0;

    @OneToMany(
        cascade = CascadeType.ALL,
        orphanRemoval = true,
        fetch = FetchType.EAGER
    )
    @JoinColumn(name = "expectation_id")
    private List<Expense> expenses = new ArrayList<>();


    public void addExpense(Expense exp){
        exp.setCategory(this.name);
        this.previsionSpending += exp.getPrice();
        expenses.add(exp);
    }

    public void removeExpense(Expense exp){
        this.previsionSpending -= exp.getPrice();
        expenses.remove(exp);
    }
    
}