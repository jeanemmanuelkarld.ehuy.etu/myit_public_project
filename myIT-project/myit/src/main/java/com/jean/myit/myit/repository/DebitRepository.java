package com.jean.myit.myit.repository;

import org.springframework.data.repository.CrudRepository;
import com.jean.myit.myit.model.Debit;

public interface DebitRepository extends CrudRepository<Debit,Integer>{

}
