package com.jean.myit.myit.repository;

import org.springframework.data.repository.CrudRepository;

import com.jean.myit.myit.model.Expectation;

public interface ExpectationRepository extends CrudRepository<Expectation,Integer>{
    
}
