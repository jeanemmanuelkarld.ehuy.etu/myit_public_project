package com.jean.myit.myit.repository;

import org.springframework.data.repository.CrudRepository;
import com.jean.myit.myit.model.Expense;

public interface ExpenseRepository extends CrudRepository<Expense,Integer>{
    
}
