package com.jean.myit.myit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jean.myit.myit.model.Image;

@Repository
public interface ImageRepository extends CrudRepository<Image,Integer>{
    
}
