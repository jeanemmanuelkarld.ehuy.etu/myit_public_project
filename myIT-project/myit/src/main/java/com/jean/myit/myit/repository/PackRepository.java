package com.jean.myit.myit.repository;

import org.springframework.data.repository.CrudRepository;

import com.jean.myit.myit.model.Pack;

public interface PackRepository extends CrudRepository<Pack,Integer>{
    
}
