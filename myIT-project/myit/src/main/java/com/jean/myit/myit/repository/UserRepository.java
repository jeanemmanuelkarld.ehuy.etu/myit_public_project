package com.jean.myit.myit.repository;

import org.springframework.data.repository.CrudRepository;

import com.jean.myit.myit.model.User;

public interface UserRepository extends CrudRepository<User,Integer>{
    
}
