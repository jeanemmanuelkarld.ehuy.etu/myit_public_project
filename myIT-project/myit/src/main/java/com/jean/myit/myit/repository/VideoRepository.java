package com.jean.myit.myit.repository;

import org.springframework.data.repository.CrudRepository;

import com.jean.myit.myit.model.Video;

public interface VideoRepository extends CrudRepository<Video,Integer>{
    
}
