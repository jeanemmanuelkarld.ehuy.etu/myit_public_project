package com.jean.myit.myit.service;

import org.springframework.stereotype.Service;

import com.jean.myit.myit.model.Debit;
import com.jean.myit.myit.repository.DebitRepository;

@Service
public class DebitService extends GenericService<Debit,DebitRepository>{

    public Debit updateDebit(Integer id,Debit deb){
        Debit current = this.get(id);
        current.setIsDebited(deb.getIsDebited());
        return this.create(current);
    }
}