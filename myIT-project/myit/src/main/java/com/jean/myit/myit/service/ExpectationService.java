package com.jean.myit.myit.service;

import java.util.List;
import org.springframework.stereotype.Service;

import com.jean.myit.myit.model.Expectation;
import com.jean.myit.myit.model.Expense;
import com.jean.myit.myit.repository.ExpectationRepository;


@Service
public class ExpectationService extends GenericService<Expectation,ExpectationRepository>{


    public Expense addExpense(Integer id,Expense exp){
        Expectation current = this.get(id);
        current.addExpense(exp);
        return exp;
    }

    public void removeExpenseById(Integer id, Integer expense_id){
        Expense exp = this.getExpenses(id).stream()
        .filter(e -> e.getId().equals(expense_id))
        .findAny().orElse(null);
        this.get(id).removeExpense(exp);
    }

    public void removeExpenseByCategory(String category, Integer expense_id){
        for (Expectation exp : this.getAll()) {
            if(exp.getName().equals(category)){
                this.removeExpenseById(exp.getId(), expense_id);
                break;
            }
        }
    }

    public List<Expense> getExpenses(Integer id){
        return this.get(id).getExpenses();
    }
    
}