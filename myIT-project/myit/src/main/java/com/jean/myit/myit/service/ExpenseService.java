package com.jean.myit.myit.service;

import org.springframework.stereotype.Service;

import com.jean.myit.myit.model.Expense;
import com.jean.myit.myit.repository.ExpenseRepository;


@Service
public class ExpenseService extends GenericService<Expense,ExpenseRepository>{

}