package com.jean.myit.myit.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;



public class GenericService<T,R extends CrudRepository<T,Integer>> {
    @Autowired
    protected R genericRepository;

    public List<T> getAll(){
        return (List) genericRepository.findAll();
    }
    public T get(Integer id){
        Optional<T> res = genericRepository.findById(id);
        if(res.isPresent())
            return res.get();
        else{
            return null;
        }
    }
    public Iterable<T> createAll(Iterable<T> items){
        return genericRepository.saveAll(items);
    }
    public T create(T item){
        return genericRepository.save(item);
    }
    public void deleteAll(Iterable<T> items){
        genericRepository.deleteAll(items);
    }
    public void delete(Integer id){
        genericRepository.deleteById(id);
    }
}
