package com.jean.myit.myit.service;

import java.io.IOException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import com.jean.myit.myit.model.Image;
import com.jean.myit.myit.repository.ImageRepository;

@Service
public class ImageService extends GenericService<Image, ImageRepository>{

    public Image uploadImage(MultipartFile image) {
        try {
            Image img = new Image();
            img.setImageData(image.getBytes());
            this.create(img);
            return img;
        } catch (IOException e) {
            return null;
        }
    }

    public byte[] getImage(@PathVariable Integer id) {
        Image image = this.get(id);
        if (image != null) {
            return image.getImageData();
        } else {
            return null;
        }
    }
    
}
