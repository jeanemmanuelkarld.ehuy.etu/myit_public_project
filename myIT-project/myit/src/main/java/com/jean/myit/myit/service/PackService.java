package com.jean.myit.myit.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.jean.myit.myit.model.Debit;
import com.jean.myit.myit.model.Expectation;
import com.jean.myit.myit.model.Image;
import com.jean.myit.myit.model.Pack;
import com.jean.myit.myit.model.User;
import com.jean.myit.myit.repository.PackRepository;

@Service
public class PackService extends GenericService<Pack, PackRepository>{

    @Autowired
    private ImageService imageService;
    @Autowired
    private UserService userService;
    @Autowired
    private DebitService debitService;
    @Autowired
    private ExpectationService expectationService;

    
    public Pack createPackWithImage(Pack pack, MultipartFile image) {
        Image uploadedImage = imageService.uploadImage(image);
        pack.setImage(uploadedImage);
        return this.create(pack);
    }

    public void cleanAll(){
        expectationService.genericRepository.deleteAll();
        debitService.genericRepository.deleteAll();
    }

    private User initializeUser(Pack pack){
        User user = new User();
        user.setBudget(pack.getBudget());
        userService.createDebitBudget(1,user);
        return user;
    }

    private List<Debit> initializeDebits(Pack pack){
        String[] lines = pack.getDebits().split("\n");
        List<Debit> debits = new ArrayList<>();
        for (String line : lines) {
            String[] debitStr = line.split(":");
            Debit deb = new Debit();
            deb.setDescription(debitStr[0]);
            deb.setCost(Double.parseDouble(debitStr[1]));
            deb.setIsDebited(false);
            debitService.create(deb);
            debits.add(deb);
        }
        return debits;
    }

    private List<Expectation> initializeExpectations(Pack pack){
        String[] lines = pack.getExpectations().split("\n");
        List<Expectation> expectations = new ArrayList<>();
        for (String line : lines) {
            String[] debitStr = line.split(":");
            Expectation deb = new Expectation();
            deb.setName(debitStr[0]);
            deb.setPrevisionExpected(Double.parseDouble(debitStr[1]));
            expectationService.create(deb);
            expectations.add(deb);
        }
        return expectations;
    }

    public void initialize(Integer id){
        this.cleanAll();
        Pack pack = this.get(id);
        this.initializeUser(pack);
        this.initializeDebits(pack);
        this.initializeExpectations(pack);
    }

    


}
