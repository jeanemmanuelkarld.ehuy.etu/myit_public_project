package com.jean.myit.myit.service;
import java.lang.Integer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.jean.myit.myit.model.User;
import com.jean.myit.myit.repository.UserRepository;


@Service
public class UserService extends GenericService<User,UserRepository>{

    @Autowired
    private ExpectationService expectationService;
    @Autowired
    private DebitService debitService;

    public User createDebitBudget(Integer id,User deb){
        User current = this.get(id);
        if(current != null){
            current.setBudget(deb.getBudget());
        }else{
            current = deb;
        }
        return this.create(current);
    }

    public User calculAndSetSavings(Integer id){
        User current = this.get(id);
        double sumOfDebits = debitService.getAll().stream().map(elm -> elm.getCost()).reduce(0.0, (elm,prev) -> elm+prev);
        double sumOfExpectations = expectationService.getAll().stream().map(elm -> elm.getPrevisionExpected()).reduce(0.0, (elm,prev) -> elm+prev);
        double savings = current.getBudget() - (sumOfDebits+sumOfExpectations);
        current.setSavings(savings);
        return this.create(current);
    };
}