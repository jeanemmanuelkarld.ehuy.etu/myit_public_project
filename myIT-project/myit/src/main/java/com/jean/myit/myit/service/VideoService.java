package com.jean.myit.myit.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.google.api.services.drive.Drive;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.jean.myit.myit.model.Video;
import com.jean.myit.myit.repository.VideoRepository;

import jakarta.servlet.ServletContext;

// import com.google.api.services.drive.Drive;
// import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import java.io.FileInputStream;
import java.io.IOException;

public class VideoService extends GenericService<Video,VideoRepository>{

    @Autowired
    private ServletContext servletContext;

    public void createPianoVideo(MultipartFile video){
        try {
            String fileName = StringUtils.cleanPath(video.getOriginalFilename());
            String uploadDir = "../" + servletContext.getRealPath("/");
            File targetLocation = new File(uploadDir + File.separator + fileName);
            video.transferTo(targetLocation);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        
    }

    public void initVideoOnDrive(MultipartFile video){
        // Chargez le fichier JSON de vos identifiants OAuth 2.0
        String credentialsPath = "chemin/vers/votre-fichier-de-credentials.json";
        ServiceAccountCredentials credentials = null;

        try (FileInputStream serviceAccountStream = new FileInputStream(credentialsPath)) {
            credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);
            // Utilisez les credentials pour construire votre service Drive
            Drive driveService = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credentials)
                .setApplicationName("Votre application Google Drive")
                .build();
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }

        
     }
    
}
