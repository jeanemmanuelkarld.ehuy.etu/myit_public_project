import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import PianoGallery from './piano_section/PianoGallery';
import ExpenseApp from './expense_section/ExpenseApp';
import ExpensePack from './expense_section/ExpensePack';


export default class App extends React.Component {
   constructor(props) {
      super(props);
   }


   render() {

      return (
         <Router>
            <Routes>
               <Route path="/" element={<ExpenseApp/>} />
               <Route path="expenseApp" element={<ExpenseApp/>} />
               <Route path="expenseApp/pack" element={<ExpensePack/>} />
               <Route path="pianoGallery" element={<PianoGallery/>} />
            </Routes>
         </Router>
      );
   }
}