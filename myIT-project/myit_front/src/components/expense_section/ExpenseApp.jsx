import Debit from './debit';
import Expense from './expense';
import Expectation from './expectation';
import Savings from './savings';
import HomeBar from '../utils/homebar';
import ButtonEnvironment from '../utils/buttonEnvironment';
import InventoryIcon from '@mui/icons-material/Inventory';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import React from 'react';
import { Link } from 'react-router-dom';
import InfoDebit from './info_debit';
import InfoExpectation from './info_expectation';


export default class ExpenseApp extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
        list : null,
         info : null,
         user : {savings : 0}
        };
      this.handleClickEnv = this.handleClickEnv.bind(this);
      this.update = this.update.bind(this);
   }

    componentDidMount(){
      this.getListOfDebits();
      this.getCurrentUser();
  }

    async getListOfDebits(){
      const response = await fetch('/debits',{method:'GET'});
      const debits = await response.json();
      this.setState({list : <Debit update={this.update}/>,info : <InfoDebit debits={debits}/>});
    }

    async getListOfexpectations(){
      const response = await fetch('/expectations',{method:'GET'});
      const expectations = await response.json();
      this.setState({list : <Expectation update={this.update}/>,info : <InfoExpectation savings={this.state.user.savings} expectations={expectations}/>});
   }

   async getCurrentUser(){
    const response = await fetch('/user',{method:'GET'});
    const user = await response.json();
    this.setState({user:user});
  }

//    async getListOfExpenses(){
//     const response = await fetch('/expenses',{method:'GET'});
//     const expenses = await response.json();
//     //this.setState({list : <Expense/>,info : <InfoExpense expenses={expenses}/>});
//  }

   update(componentStr){
    if(componentStr === "debit")
        this.getListOfDebits();
    if(componentStr === "expectation")
        this.getListOfexpectations();
    if(componentStr === "expense")
        this.getListOfExpenses();
    this.getCurrentUser();
   }

   handleClickEnv(event){
      if(event.target.id === "debit")
          this.getListOfDebits();
      if(event.target.id === "expense")
        this.setState({list : <Expense/>,info : ""});
      if(event.target.id === "expectation")
          this.getListOfexpectations();
   }

   render() {
    return (
      <div className='app'>
        <HomeBar/>
        <div style={{ backgroundImage : 'url(/images/terrasse.jpg)' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
                <div style={{ marginLeft: '70%' }}>
                <ButtonEnvironment onClick={this.handleClickEnv}/>
                </div>
                <div style={{ marginLeft: '300px' }}>
                <Stack direction="row" spacing={2}>
                      <Button
                        variant="contained"
                        sx={{ backgroundColor: 'brown' }}
                        endIcon={<InventoryIcon />}
                        component={Link} to="/expenseApp/pack"
                      >
                        Pack
                      </Button>
                    </Stack>
                </div>

            </div>
        </div>

        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
      
            <div style={{ display: 'flex', alignItems: 'center' }}>
                <Savings user={this.state.user} style={{ marginRight: '10px' }}/>
                <div style={{ marginLeft: '50px' }}>
                    {this.state.list}
                </div>
            </div>
        </div>
      </div>
            <div>
                  {this.state.info}
              </div>
      </div>
    );
  }
  
  
  
  
}
