
import HomeBar from '../utils/homebar';
import ButtonPackEnv from '../utils/buttonPackEnv';
import React from 'react';
import PackCreate from './pack_create';
import PackChoose from './pack_choose';



export default class ExpenseApp extends React.Component {
   constructor(props) {
      super(props);
      this.state = {list : null};
      this.handleClickEnv = this.handleClickEnv.bind(this);
   }

   handleClickEnv(event){
      if(event.target.id === "create")
          this.setState({list : <PackCreate/>});
      if(event.target.id === "choose")
            this.setState({list : <PackChoose/>});
   }


   render() {
    return (
      <div className='app'>
        <HomeBar/>
        <ButtonPackEnv onClick={this.handleClickEnv}/>
        {this.state.list}
       </div>
    );
  }
} 