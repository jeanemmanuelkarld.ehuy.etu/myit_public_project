import List from '../utils/list';
import React from 'react';
import {Container} from 'reactstrap';
import PriceInput from '../utils/priceInput';
import ButtonDelete from '../utils/buttonDelete';
import ButtonAdd from '../utils/buttonAdd';
import LargeTextField from '../utils/LargeTextField';
import MoneyOffIcon from '../utils/moneyOffIcon';
import MoneyOnIcon from '../utils/moneyOnIcon'; 
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';


export default class Debit extends React.Component {
   constructor(props) {
      super(props);
      this.state = {debits : [], display_list : null, prevDebitState : false};
      this.addDebit = this.addDebit.bind(this);
      this.deleteDebit = this.deleteDebit.bind(this);
      this.updateDebit = this.updateDebit.bind(this);
   }

   // private Integer id;
   // private String description;
   // private Double cost;
   // @Column(name = "is_debited")
   // private Boolean isDebited = false;

   componentDidMount(){
      this.getListOfDebits();
   }

   async getListOfDebits(){
      const response = await fetch('/debits',{method:'GET'});
      const debits = await response.json();
      const columns = this.initColumns();
      const rows = this.initRows(debits);
      this.setState({debits : debits,
         display_list : <List
                           rows={rows}
                           columns={columns}
                        /> 
   });
   this.props.update("debit");
   }

   

   initColumns(){
      const columns = [
         { id: 'description', label: 'Description', minWidth: 170,
         format : v => v },
         { id: 'cost', label: 'Cost', minWidth: 100,
         format : v => v },
         { id: 'debitStatus', label: 'Status of Debit', minWidth: 50,
          format : val => this.formatDebitStatus(val)
         },
         { id: 'action', label: 'Action', minWidth: 50,
          format : val => (val.desc === "ajouter") ? <ButtonAdd onClick={this.addDebit}/> : <ButtonDelete id={val.id} onClick={this.deleteDebit}/> 
          },
       ];
      return columns;
   }

   formatDebitStatus(val){
      this.setState({prevDebitState : val.isDebited});
      if(val.action === "create")
            return <MonetizationOnIcon/>;
      if(!val.isDebited){
         return <MoneyOnIcon id={val.id} updateDebit={this.updateDebit}/>;
      }else{
         return <MoneyOffIcon id={val.id} updateDebit={this.updateDebit}/>;
      }
   }

   initRows(data){
      const createSection = this.createDebitData();
      const rows = data.map(debit => this.createData(debit.id,debit.description,debit.cost,debit,{desc : "delete", id : debit.id}));
      const res = [createSection,...rows];
      return res;
   }

   createData(id,description, cost, debitStatus,action) {
      return {id,description, cost, debitStatus,action};
   }

   createDebitData(){
      const id = "createDebit";
      const description = <LargeTextField/>;
      const cost = <PriceInput/>;
      const debitStatus = {isDebited:false,action:"create"};
      const action = {desc : "ajouter", id : id};
      return {id,description, cost, debitStatus,action};
   }

   async addDebit(){
      let cost = document.getElementById('filled-adornment-amount').value;
      cost = (cost === '') ? (0.0) : parseFloat(cost);

      const debitObject = {
         description : document.getElementById('fullWidth').value,
         cost : cost,
         isDebited : false
      };

      const body = JSON.stringify(debitObject);
      const requestOptions = {
         method : 'POST',
         headers : { "Content-Type": "application/json" },
         body : body
      };
      const response = await fetch("/debit",requestOptions);
      this.update();
      
   }

   async updateDebit(id){
      
      const debitObject = {
         isDebited : !this.state.prevDebitState
      };

      this.setState(prevstate => ({ prevDebitState : !prevstate.prevDebitState}));
      
      const body = JSON.stringify(debitObject);
      const requestOptions = {
         method : 'PUT',
         headers : { "Content-Type": "application/json" },
         body : body
      };
      const response = await fetch(`/debit/${id}`,requestOptions);
      
      this.update();
      
   }

    async deleteDebit(event){
      const id = event.target.id;
      const response = await fetch(`/debit/${id}`, {method:'DELETE'});
      this.update();
    }

   update(){
      this.getListOfDebits();
   }

   render() {
      return (
         <div className='app'>
            <div>
               <Container fluid>
               <h3>List of all your debits</h3>
               {this.state.display_list}
               </Container>
            </div>
         </div>

      );
   }
}
