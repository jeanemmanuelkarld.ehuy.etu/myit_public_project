import List from '../utils/list';
import React from 'react';
import {Container} from 'reactstrap';
import PriceInput from '../utils/priceInput';
import ButtonDelete from '../utils/buttonDelete';
import ButtonAdd from '../utils/buttonAdd';
import LargeTextField from '../utils/LargeTextField';
import Slider from '../utils/Slider';


export default class expectation extends React.Component {
   constructor(props) {
      super(props);
      this.state = {expectations : [], display_list : null, expectations : []};
      this.addExpectation = this.addExpectation.bind(this);
      this.deleteExpectation = this.deleteExpectation.bind(this);
   }

   // private Integer id;
   // private String name;
   // private Double previsionExpected;
   // private Double previsionSpending= (double) 0;
   // private List<Expense> expenses = new ArrayList<>();

    async componentDidMount(){
      await this.getListOfExpectation();
      this.getListOfexpectations();
   }

   async getListOfExpectation(){
      const response = await fetch('/expectations',{method:'GET'});
      const expectations = await response.json();
      this.setState({
         expectations : expectations
   });
   }

   async getListOfexpectations(){
      const response = await fetch('/expectations',{method:'GET'});
      const expectations = await response.json();

      const columns = this.initColumns();
      const rows = this.initRows(expectations);
      this.setState({expectations : expectations,
         display_list : <List
                           rows={rows}
                           columns={columns}
                        /> 
   });
   this.props.update("expectation");
   }

   initColumns(){
      const columns = [
         { id: 'name', label: 'Name', minWidth: 170,
         format : v => v },
         { id: 'previsionSpending', label: 'previsionSpending / previsionExpected', minWidth: 50,
          format : v => v},
         { id: 'previsionExpected', label: 'previsionExpected', minWidth: 100,
         format : v => v },
         { id: 'action', label: 'Action', minWidth: 50,
          format : val => (val.desc === "ajouter") ? <ButtonAdd onClick={this.addExpectation}/> : <ButtonDelete id={val.id} onClick={this.deleteExpectation}/> 
          },
       ];
      return columns;
   }

   initRows(data){
      const createSection = this.createexpectationData();
      
      const rows = data.map(expectation => this.createData(expectation.id,expectation.name,<Slider maxValue={expectation.previsionExpected} defaultValue={expectation.previsionSpending}/>,expectation.previsionExpected,{desc : "delete", id : expectation.id}));
      const res = [createSection,...rows];
      return res;
   }

   createData(id,name,previsionSpending,previsionExpected,action) {
      return {id,name,previsionSpending,previsionExpected,action};
   }

    createexpectationData(){
      const id = "createexpectation";
      const name = <LargeTextField/>;
      const previsionSpending = "";
      const previsionExpected = <PriceInput/>;
      const action = {desc : "ajouter", id : id};
      return {id,name,previsionSpending,previsionExpected,action};
   }

   async addExpectation(){
      let cost = document.getElementById('filled-adornment-amount').value;
      cost = (cost === '') ? (0.0) : parseFloat(cost);

   // private String name;
   // private Double previsionExpected;
   // private Double previsionSpending= (double) 0;
   // private List<Expense> expenses = new ArrayList<>();

      const expectationObject = {
         name : document.getElementById('fullWidth').value,
         previsionExpected : cost,
         previsionSpending : 0.0
      };

      const body = JSON.stringify(expectationObject);
      const requestOptions = {
         method : 'POST',
         headers : { "Content-Type": "application/json" },
         body : body
      };
      
      const response = await fetch(`/expectation`,requestOptions);
      const expectation = await response.json();
     
      this.update();
   }

    async deleteExpectation(event){
      const id = event.target.id;
      await fetch(`/expectation/${id}`, {method:'DELETE'});
      this.update();
    }

   update(){
      this.getListOfexpectations();
   }

   render() {
      return (
         <div className='app'>
            <div>
               <Container fluid>
               <h3>List of all your expectations</h3>
               {this.state.display_list}
               </Container>
            </div>
         </div>

      );
   }
}
