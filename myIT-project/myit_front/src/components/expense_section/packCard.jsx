import React from 'react';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import ButtonDelete from '../utils/buttonDelete';
import { CardActionArea } from '@mui/material';

export default class PackCard extends React.Component {
    constructor(props) {
       super(props);
       this.getId = this.getId.bind(this);
       this.deletePack = this.deletePack.bind(this);
    }

    getId(){
      this.props.handleClick(this.props.id);
    }

    deletePack(){
      this.props.deletePack(this.props.id);
    }

    render() {
        return (
          <div onClick={this.getId} id={this.props.id}>
            <Card sx={{ maxWidth: 300, height:500,boxShadow:15 }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="400"
                image={this.props.src}
                alt={this.props.title}
              />
               <Typography  variant="h4" component="div" color="black">
                  {this.props.title}
                </Typography>
            </CardActionArea>
            <ButtonDelete id={this.props.id} onClick={this.deletePack}/>
          </Card>  
          </div>
        );
     }
}