import Video from './video';
import HomeBar from '../utils/homebar';
import Typography from '@mui/material/Typography';

import React from 'react';


export default class PianoGallery extends React.Component {
   constructor(props) {
      super(props);
   }

   render() {
      return (
         <div className='piano_gallery'>
            <HomeBar/>
            <div style={{backgroundColor : "black"}}>
            <Typography gutterBottom variant="h1" component="div" 
               sx={{
                  backgroundImage: "linear-gradient(120deg, blue, violet)",
                  WebkitBackgroundClip: "text",
                  WebkitTextFillColor: "transparent",
                  textAlign : 'center'
               }}>
               Maestro Maishima
            </Typography>
            </div>
            
            <Video videoSrc="/videos/test.mp4" />
         </div>
      );
   }
}   