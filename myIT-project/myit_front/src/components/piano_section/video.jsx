import React from 'react';
import VideoInfo from './videoinfo.jsx';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import Button from '@mui/material/Button';


export default class Video extends React.Component {
    constructor(props) {
       super(props);
    }

    render() {
        return (
            <Card className='video' 
            sx={{
               maxWidth: 300,
               maxHeight: 500,
               backgroundColor:'black'
                }}>
               <video width="400" height="250" controls>
                     <source src={this.props.videoSrc} type="video/mp4"/>
               </video>
               <VideoInfo
                  songTitle="test" 
                  duration={19} 
                  trainingSession={3}
               />
               <CardActions backgroundColor="white">
                  <Button size="small">Action 1</Button>
                  <Button size="small">Action 2</Button>
               </CardActions>
            </Card>     
        );
     }
}