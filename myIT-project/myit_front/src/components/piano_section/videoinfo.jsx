import React from 'react';
import Typography from '@mui/material/Typography';
import CardContent from '@mui/material/CardContent';

export default class VideoInfo extends React.Component {
    constructor(props) {
       super(props);
    }

    render() {
        return (
            <CardContent 
            sx={{ 
               maxWidth: 350,
               maxHeight: 100
               }}>
                  <Typography gutterBottom variant="h5" component="div" color="darkcyan">
                     Details
                  </Typography>
                  <Typography variant="body2" color="whitesmoke">
                     Song title : {this.props.songTitle}
                  </Typography>
                  <Typography variant="body2" color="whitesmoke">
                     Learning duration : {this.props.duration} houres
                  </Typography>
                  <Typography variant="body2" color="whitesmoke">
                     Training Session: {this.props.trainingSession}
                  </Typography>
               </CardContent>
          );
     }
}