import React, { Component } from 'react';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import Stack from '@mui/material/Stack';

class IconLabelButtons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // Initial state properties if needed
    };
  }

  render() {
    return (
      <Stack direction="row" spacing={2}>
        <Button
          onClick={this.props.onClick}
          id={this.props.id}
          variant="outlined"
          color="error"
          startIcon={<DeleteIcon />}
        >
          Delete
        </Button>
      </Stack>
    );
  }
}

export default IconLabelButtons;
