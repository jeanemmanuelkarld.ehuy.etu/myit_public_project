import React, { Component } from 'react';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';

class MoneyOnIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
    this.getId = this.getId.bind(this);
  }

  getId(){
    this.props.updateDebit(this.props.id);
  }

  render() {
    return (
      <div>
        <MonetizationOnIcon id={this.props.id} onClick={this.getId}/>
      </div>
    );
  }
}

export default MoneyOnIcon;