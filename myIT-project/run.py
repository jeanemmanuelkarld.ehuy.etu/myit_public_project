import subprocess

# Commande à exécuter dans le premier terminal
commande_terminal_1 = "cd ~/Desktop/learning/myIT-project;make"

# Commande à exécuter dans le second terminal
commande_terminal_2 = "cd ~/Desktop/learning/myIT-project;make run-react"

# Ouvrir le premier terminal et exécuter la première commande
subprocess.Popen(['osascript', '-e', f'tell application "Terminal" to do script "{commande_terminal_1}"'])

# Ouvrir le second terminal et exécuter la seconde commande
subprocess.Popen(['osascript', '-e', f'tell application "Terminal" to do script "{commande_terminal_2}"'])
